/**
 * Created by Kasso on 29.05.2017.
 */
// $(document).ready(function(){
//     $('.owl-carousel').owlCarousel({
//         items: 1,
//         loop: true,
//         nav: true,
//         // autoplay: true,
//         // autoplayTimeout: 5000,
//         // autoplaySpeed: 450,
//         // autoplayHoverPause: false,
//         // navText: [
//         //     '<div class="prev"></div>',
//         //     '<div class="next"></div>'
//         // ]
//     });
// });

var btn_prev = document.querySelector('.slider .btn_prev');
var btn_next = document.querySelector('.slider .btn_next');

var someimages = document.querySelectorAll('.photos img');

var i = 0;

btn_prev.onclick = function(){
    someimages[i].style.display = 'none';

    i--;

    if ( i < 0) {
        i = someimages.length - 1;
    }

    someimages[i].style.display = 'block';
};

btn_next.onclick = function(){
    someimages[i].style.display = 'none';

    i++;

    if ( i >= someimages.length ) {
        i = 0;
    }

    someimages[i].style.display = 'block';
};

//pagination
var list = document.querySelector('.photos');

var newDivForDot = document.createElement('ul');
    newDivForDot.style.display = 'block';
    newDivForDot.style.position = 'absolute';
    newDivForDot.style.bottom = '50px';
    newDivForDot.style.left = '50%';
    newDivForDot.style.transform = 'translateX(-50%)';



list.appendChild(newDivForDot);




// var newLiForDot = document.createElement('li');
// newLiForDot.style.display = 'inline-block';
// newLiForDot.style.width = '14px';
// newLiForDot.style.height = '14px';
// newLiForDot.style.borderRadius = '50%';
// newLiForDot.style.backgroundColor = 'white';
// newLiForDot.style.margin = '0px 15px 0px 15px';
//
// newDivForDot.appendChild(newLiForDot);



// var newLiForDot2 = newLiForDot.cloneNode(true);
// newLiForDot.parentNode.insertBefore(newLiForDot2, newLiForDot.nextSibling);
//
// var newLiForDot3 = newLiForDot2.cloneNode(true);
// newLiForDot2.parentNode.insertBefore(newLiForDot3, newLiForDot2.nextSibling);





for(var k = 0; k < someimages.length; k++) {
    var newLiForDot = document.createElement('li');
    newLiForDot.className = k;
    newLiForDot.style.display = 'inline-block';
    newLiForDot.style.width = '14px';
    newLiForDot.style.height = '14px';
    newLiForDot.style.borderRadius = '50%';
    newLiForDot.style.backgroundColor = 'white';
    newLiForDot.style.margin = '0px 15px 0px 15px';

    newDivForDot.appendChild(newLiForDot);
}

var dots = document.querySelectorAll('.photos li');

for(var n = 0; n < dots.length; n++) {

    var dot = dots[n];
    dot.onclick = function () {
        for(var l = 0; l < someimages.length; l++) {
            someimages[l].style.display = 'none';
        }

        // console.log(this.className)
        someimages[this.className].style.display = 'block';
    }
}

// $('.photos li').on('click', function () {
//     var dotIndex = $(this).index();
//     $('.photos img').hide(100);
//     $('.photos img').eq(dotIndex).show(500);
// });






// list += '</ul>';
//
//     document.querySelector('.photos').appendChild(list);


